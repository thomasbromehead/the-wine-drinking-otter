class ApplicationMailer < ActionMailer::Base
  default from: 'laloutre@qoqa.ch'
  layout 'mailer'
end
