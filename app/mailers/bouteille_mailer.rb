class BouteilleMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.bouteille_mailer.updated.subject
  #
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.bouteille_mailer.created.subject
  #
  def created(bouteille)
    subject = "Bouteille #{bouteille.name} crée avec succès"
    mail to: "to@example.org", subject: subject
  end
end
