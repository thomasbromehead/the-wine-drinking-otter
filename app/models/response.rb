class Response
  ValidStatus = (200..599)
  InvalidResponse = Class.new(StandardError)
  Created = "Bouteille Created".freeze
  UpdatedSuccessfully = "Bouteille updated successfully".freeze

  class PayloadValidator < ActiveModel::Validator
    def validate(record)
      record.errors(:message).add("Invalid JSON response") unless JSON.parse(record.message).all?
    end
  end

  attr_accessor :status, :message
  include ActiveModel::Validations
  validates_presence_of(:status, :message)
  validates_numericality_of :status, greater_than_or_equal_to: ValidStatus.min, less_than_or_equal_to: ValidStatus.max
  validates_with PayloadValidator
end
