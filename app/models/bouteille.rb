# == Schema Information
#
# Table name: bouteilles
#
#  id          :integer          not null, primary key
#  chateau_id  :integer          not null
#  appelation  :string
#  description :text
#  viticulture :string
#  cepage      :string
#  elevage     :string
#  millesime   :string
#  type        :integer
#  garde       :string
#  titrage     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  contenance  :integer
#
class Bouteille < ApplicationRecord
  belongs_to :chateau
  attr_accessor :fiche_de_degustation
  enum wine_type: {rouge: 0, blanc: 1, rose: 2}
  validates :wine_type, inclusion: {in: wine_types.keys}
  after_save :send_notification

  private

  def send_notification
    SendCreateNotificationJob.perform_later(self)
  end
end
