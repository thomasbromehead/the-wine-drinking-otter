# == Schema Information
#
# Table name: chateaus
#
#  id         :integer          not null, primary key
#  name       :string
#  region     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Chateau < ApplicationRecord
end
