class Api::V1::BouteillesController < ApplicationController
  before_action :check_content_type, only: [:create, :update]
  before_action :load_bouteille, only: [:destroy, :show, :update]
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_bouteille
  ActionController::Parameters.action_on_unpermitted_parameters = :raise

  InvalidRequest = Class.new(StandardError)

  JsonRequest = "application/json".freeze


  def show
    metadata = prepare_response_metadata
    payload = metadata[:channel].merge({item: @bouteille.attributes})
    response = respond_with do |r|
      r.message = {channel: payload}.to_json
      r.status = 200
      r
    end
    if response.valid?
      render json: response.message, status: response.status
    else
      raise Response::InvalidResponse.new(%{
                                    Please check your response, it has the following errors:
                                    #{response.errors.full_messages}
                                })
    end
  end


  def create
    build_bouteille
    if @bouteille.save
# Mail with separate Thread can fail and won't be retried so moved to a separate job in after_save, thanks Jonathan!
=begin
      t = Thread.new(@bouteille) do
          BouteilleMailer.created(@bouteille).deliver_now
      end
      t.join()
=end
      response = respond_with do |r|
          r.message = {success: Response::Created}.to_json
          r.status = 201
          r
        end
      if response.valid?
        render json: response.message, status: response.status and return
      else
        render json: {error: "Invalid response", message: response.errors }, location: root_path, status: 500
      end
    else
      render json: {error: @bouteille.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    instance_variable_get(:@bouteille).destroy if instance_variable_defined?(:@bouteille)
    respond_with do
      head :ok
    end
  end

  def index
    response = respond_with do |r|
      r.message = bouteilles_to_json(bouteilles_scope)
      r.status = 200
      r
    end
    if response.valid?
      render json: response.message,  status: response.status and return
    else
      raise Response::InvalidResponse.new(%{
                                    Please check your response, it has the following errors:
                                    #{response.errors.full_messages}
                                          })
    end
  end

  def update
    if @bouteille.update(bouteille_params)
      response = respond_with do |r|
        r.status = 201
        r.message = {success: Response::UpdatedSuccessfully, bouteille: @bouteille.attributes}.to_json
        r
      end
      if response.valid?
        render json: response.message, status: response.status and return
      else
        render json: {error: "Invalid response", message: response.errors }, location: root_path, status: 500 and return
      end
    else
      render json: {error: @bouteille.errors}, status: :unprocessable_entity and return
    end
  end

  private

  def load_bouteille
    @bouteille ||= Bouteille.find(params[:id])
  end

  def bouteilles_to_json(bouteilles)
    bouteilles.to_a.to_json
  end

  def respond_with
    response = yield Response.new
    response
  end

  def bouteilles_scope
    Bouteille.all
  end

  def bouteille_params
    params.require(:bouteille).permit(:name,
                                      :description,
                                      :viticulture,
                                      :cepage,
                                      :elevage,
                                      :millesime,
                                      :wine_type,
                                      :garde,
                                      :appelation,
                                      :titrage,
                                      :chateau_id
                                    )
  end

  def build_bouteille
    @bouteille ||= bouteilles_scope.build
    @bouteille.attributes = bouteille_params
  end

  def invalid_bouteille
    render json: {error: "Could not find bouteille with id: #{params[:id]}"}, status: 404
  end

  def prepare_response_metadata
    {
      channel: {
        title: "Produit du jour sur Qwine",
        description: "Chaque jour nos experts se décarcassent pour vous trouver les meilleurs bons plans.",
        link: "https://qwine.qoqa.ch",
        language: "fr",
      }
    }.with_indifferent_access
  end

  def check_content_type
    if request.headers["Content-Type"].include?(JsonRequest)
      true
    else
      raise InvalidRequest.new("Invalid Content-Type provided")
    end
  end
end