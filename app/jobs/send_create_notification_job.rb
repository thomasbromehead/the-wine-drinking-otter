class SendCreateNotificationJob < ApplicationJob
  queue_as :default

  def perform(bouteille)
    BouteilleMailer.created(bouteille).deliver_now
  end
end
