class AddContenanceToBouteille < ActiveRecord::Migration[6.1]
  def change
    add_column :bouteilles, :contenance, :integer
  end
end
