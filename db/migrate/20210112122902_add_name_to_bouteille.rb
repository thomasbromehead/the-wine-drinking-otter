class AddNameToBouteille < ActiveRecord::Migration[6.1]
  def change
    add_column :bouteilles, :name, :string
  end
end
