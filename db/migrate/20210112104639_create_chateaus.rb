class CreateChateaus < ActiveRecord::Migration[6.1]
  def change
    create_table :chateaus do |t|
      t.string :name
      t.string :region

      t.timestamps
    end
  end
end
