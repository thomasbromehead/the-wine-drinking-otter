class ChangeTypeColumnNameOnBouteille < ActiveRecord::Migration[6.1]
  def change
    rename_column :bouteilles, :type, :wine_type
  end
end
