class AddPriceToBouteille < ActiveRecord::Migration[6.1]
  def change
    add_column :bouteilles, :price, :decimal
  end
end
