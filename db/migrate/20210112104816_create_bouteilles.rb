class CreateBouteilles < ActiveRecord::Migration[6.1]
  def change
    create_table :bouteilles do |t|
      t.references :chateau, null: false, foreign_key: true
      t.string :appelation
      t.text :description
      t.string :viticulture
      t.string :cepage
      t.string :elevage
      t.string :millesime
      t.integer :type
      t.string :garde
      t.string :titrage

      t.timestamps
    end
  end
end
