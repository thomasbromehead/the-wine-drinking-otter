# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
regions = {
  Chateau1: "Tessin, Suisse",
  Chateau2: "Sardaigne, Italie",
  Chateau3: "Provence, France"
}.with_indifferent_access

3.times do |i|
  begin
    chateau = Chateau.create!(name: "Chateau#{i}", region: regions["Chateau#{i}"])
    Rails.logger.info("Creating Chateau: #{chateau.name}")
  rescue  ActiveRecord::RecordInvalid => e
    Rails.logger.error(e.message)
  end
end

Urls = "https://www.divo.ch/fr/shop-vin/france/bourgogne/beaune-greves-0"
#  chateau_id  :integer          not null
#  appelation  :string
#  description :text
#  viticulture :string
#  cepage      :string
#  elevage     :string
#  millesime   :string
#  type        :integer
#  garde       :string
#  titrage     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  contenance  :integer

chateau1, chateau2, chateau3 = [Chateau.first, Chateau.second, Chateau.last]

names = {
  "Agra, Collina d'Oro DOC" => "https://www.divo.ch/fr/shop-vin/suisse/tessin/agra-collina-doro-merlot-riserva-2",
  "Sassarei, Merlot DOC" => "https://www.divo.ch/fr/shop-vin/suisse/tessin/sassarei-merlot-0",
  "Lamone, Merlot Riserva" => "https://www.divo.ch/fr/shop-vin/suisse/tessin/lamone-merlot-riserva-0",
  "Ala Blanca" => "https://www.divo.ch/fr/shop-vin/italie/sardaigne/ala-blanca-0",
  ""=>"",
}

bouteille1 = {
  description: "Vinification et élevage: 2 ans d'élevage en barriques, 60% neuves.",
  name: "Agra, Collina d'Oro DOC",
  appelation: "Ticino DOC",
  chateau_id: chateau1.id,
  cepage: "Merlot 100%",
  elevage: "Barrique",
  millesime: "2018",
  garde: "2029",
  titrage: "13.5%",
  wine_type: 0,
  viticulture: "Viticulture conventionnelle",
  contenance: 75,
  price: 90
}

fiche_de_degustation_1 = {
  nez: "pruneau frais, cassis et framboise, avec juste un soupçon de moka et de fumée froide (la moitié du vin a été élevé en fût de 2e et 3e passage), pour un nez intense et vif.",
  bouche: "frais, fin et croquant en bouche. Acidité juteuse et tannins légèrement rustiques, final fruité et aromatique, jolie persistance. Classique et très bien fait.",
  robe: "rouge rubis, brillant"
}

bouteille2 = {
  description: "Merlot del Ticino DOC Cantina Pelossi 2019",
  name: "Sassarei, Merlot DOC",
  appelation: "Ticino DOC",
  chateau_id: chateau1.id,
  cepage: "Merlot 100%",
  elevage: "Barrique",
  millesime: "2019",
  garde: "2026",
  titrage: "12.5%",
  wine_type: 0,
  viticulture: "Viticulture conventionnelle",
  contenance: 75,
  price: 67
}

fiche_de_degustation_2 = {
  nez: "pruneau frais, cassis et framboise, avec juste un soupçon de moka et de fumée froide (la moitié du vin a été élevé en fût de 2e et 3e passage), pour un nez intense et vif.",
  bouche: "frais, fin et croquant en bouche. Acidité juteuse et tannins légèrement rustiques, final fruité et aromatique, jolie persistance. Classique et très bien fait.",
  robe: "rouge rubis, brillant"
}

bouteille3 = {
  description: "Merlot del Ticino DOC Cantina Pelossi 2019",
  name: "Sassarei, Merlot DOC",
  appelation: "Ticino DOC",
  chateau_id: chateau1.id,
  cepage: "Merlot 100%",
  elevage: "Barrique",
  millesime: "2018",
  garde: "2033",
  titrage: "13.5%",
  wine_type: 0,
  viticulture: "Viticulture conventionnelle",
  contenance: 75,
  price: 88
}

fiche_de_degustation_3 = {
  nez: "intense et complexe, la violette, le cassis et la prune se mêlent aux notes d'élevage, moka, chocolat noir, cendre, crème anglaise, sans que les uns prennent le pas sur les autres. Un modèle d'harmonie et de précision.",
  bouche: "attaque puissante et veloutée, les tannins, denses, caressent le palais et la matière est portée par une belle acidité qui prolonge une fin de bouche raffinée, sur les épices et les fruits noirs.",
  robe: "rubis soutenu, reflets violets."
}

bouteille4 = {
  description: "Vermentino di Sardegna DOC",
  name: "Ala Blanca",
  appelation: "Vermentino di Sardegna DOC",
  chateau_id: chateau2.id,
  cepage: "Vermentino",
  elevage: "Barrique",
  millesime: "2018",
  garde: "2033",
  titrage: "13.5%",
  wine_type: 0,
  viticulture: "Bio",
  contenance: 75,
  price: 98
}

fiche_de_degustation_4 = {
  nez: "arômes de fleurs blanches, fins, agrumes, tilleul.",
  bouche: "une attaque fraîche, longiligne, harmonieuse, trame fine aux arômes d'agrumes, de réglisse, d'amande, une pointe saline",
  robe: "jaune clair, brillante, reflets verdâtres."
}

bouteille5 = {
  description: "Vermentino di Sardegna DOC Poderi Parpinello",
  name: "Sessantaquattro",
  appelation: "Vermentino di Sardegna DOC",
  chateau_id: chateau2.id,
  cepage: "Vermentino",
  elevage: "Barrique",
  millesime: "2017",
  garde: "2022",
  titrage: "13.5%",
  wine_type: 1,
  viticulture: "Viticulture conventionnelle",
  contenance: 75,
  price: 79
}

fiche_de_degustation_5 = {
  nez: "arômes de fleurs blanches, fins, agrumes, tilleul.",
  bouche: "une attaque fraîche, longiligne, harmonieuse, trame fine aux arômes d'agrumes, de réglisse, d'amande, une pointe saline",
  robe: "jaune clair, brillante, reflets verdâtres."
}

bouteille6 = {
  description: "Cannonau di Sardegna DOC Poderi Parpinello 2015",
  name: "Riserva",
  appelation: "Cannonau di Sardegna DOC",
  chateau_id: chateau2.id,
  cepage: "Cannonau (Garnacha, Grenache) 100 %",
  elevage: "Barrique",
  millesime: "2015",
  garde: "2025",
  titrage: "14.5%",
  wine_type: 0,
  viticulture: "Viticulture conventionnelle",
  contenance: 75,
  price: 134,
}

fiche_de_degustation_6 = {
  nez: "intense, arômes de fruits à noyaux, d'épices douces, de cannelle, de fraise cuite.",
  bouche: %{
  une première bouche en solaire, la force est tempérée par une belle fraîcheur et l'identité terroir volcanique. Des tanins fins et ronds sont enrobés par la charpente chaleureuse et une
  pointe de CO2 renforce la fraîcheur du vin. Une finale épicée, de figue, saline, de réglisse, de baies noires et d'eucalyptus"},
  robe: "rubis foncé avec des reflets cerise."
}

bouteille7 = {
  description: "Bandol AOC Domaine Tempier 2019",
  name: "Bandol Rosé",
  appelation: "Bandol AOC",
  chateau_id: chateau3.id,
  cepage: "Mourvèdre (Monastrell) 50 %, Grenache Noir (Garnacha Tinta) 28 %, Cinsault 20 %, Carignan (Mazuelo) 2 %",
  elevage: "Barrique",
  millesime: "2019",
  garde: "2029",
  titrage: "13.5%",
  wine_type: 2,
  viticulture: "Viticulture conventionnelle",
  contenance: 100,
  price: 85,
}

fiche_de_degustation_7 = {
  nez: "arômes fumés et d'air marin qui ont besoin de s'oxygéner, pour révéler des notes de framboise, de pamplemousse pressé et d'écorce d'orange, avec une pointe de cynorrhodon.",
  bouche: %{
beaucoup de matière, de texture aussi, agrumes, petits fruits rouges avec une pincée de poivre noir. Long final d'amande grillée et de zeste de pamplemousse.},
  robe: "saumon, reflets orangé"
}

bouteille8 = {
  description: "Bandol AOC Domaine Tempier 2017",
  name: "La Tourtine",
  appelation: "Bandol AOC",
  chateau_id: chateau3.id,
  cepage: "Mourvèdre (Monastrell) 50 %, Grenache Noir (Garnacha Tinta) 28 %, Cinsault 20 %, Carignan (Mazuelo) 2 %",
  elevage: "Barrique",
  millesime: "2017",
  garde: "2037",
  titrage: "15%",
  wine_type: 2,
  viticulture: "Viticulture conventionnelle",
  contenance: 150,
  price: 97,
}

fiche_de_degustation_8 = {
  nez: %{profond avec une pureté des arômes, baies noires, cacao, tabac, de cuir, d'épices, de poivre et de réglisse},
  bouche: %{
une attaque en finesse, puis le volume se développe, structure longiligne soutenue par des tanins de haute couture, un toucher de bouche d'une grande délicatesse,
une texture fraîche et noble. Finale sur l'émotion!},
  robe: "foncée, cerise noire, reflets rose.",
}

bouteille9 = {
  description: "Bandol AOC Domaine Tempier 2017",
  name: "Cabassaou",
  appelation: "Bandol AOC",
  chateau_id: chateau3.id,
  cepage: "Mourvèdre (Monastrell) 95 %, Syrah 4 %, Cinsault 1 %",
  elevage: "Barrique",
  millesime: "2017",
  garde: "2040",
  titrage: "15%",
  wine_type: 2,
  viticulture: "Viticulture conventionnelle",
  contenance: 150,
  price: 134,
}

fiche_de_degustation_9 = {
  nez: %{profond avec une pureté des arômes, baies noires, cacao, tabac, de cuir, d'épices, de poivre et de réglisse},
  bouche: %{
une attaque en finesse, puis le volume se développe, structure longiligne soutenue par des tanins de haute couture, un toucher de bouche d'une grande délicatesse,
une texture fraîche et noble. Finale sur l'émotion!},
  robe: "foncée, cerise noire, reflets rose.",
}



[bouteille1, bouteille2, bouteille3, bouteille4, bouteille5, bouteille6, bouteille7, bouteille8, bouteille9].each do |bouteille|
  begin
    b = Bouteille.create!(bouteille)
    Rails.logger.info("Created Bouteille: #{b.name}")
  rescue ActiveRecord::RecordInvalid => e
    Rails.logger.error(e.errors.full_messages)
  end
end