# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_12_122902) do

  create_table "bouteilles", force: :cascade do |t|
    t.integer "chateau_id", null: false
    t.string "appelation"
    t.text "description"
    t.string "viticulture"
    t.string "cepage"
    t.string "elevage"
    t.string "millesime"
    t.integer "wine_type"
    t.string "garde"
    t.string "titrage"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "contenance"
    t.decimal "price"
    t.string "name"
    t.index ["chateau_id"], name: "index_bouteilles_on_chateau_id"
  end

  create_table "chateaus", force: :cascade do |t|
    t.string "name"
    t.string "region"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "bouteilles", "chateaus"
end
