# == Route Map
#

Rails.application.routes.draw do
  root to: "api/v1/bouteilles#index"
  scope module: 'api' do
    scope module: 'v1' do
      constraints format: :json do
        resources :bouteilles
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
