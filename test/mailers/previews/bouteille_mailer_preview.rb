# Preview all emails at http://localhost:3000/rails/mailers/bouteille_mailer
class BouteilleMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/bouteille_mailer/updated
  def updated
    BouteilleMailer.updated
  end

  # Preview this email at http://localhost:3000/rails/mailers/bouteille_mailer/created
  def created
    BouteilleMailer.created
  end

end
