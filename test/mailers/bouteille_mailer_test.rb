require "test_helper"

class BouteilleMailerTest < ActionMailer::TestCase
  fixtures(:bouteilles)
  test "created" do
    mail = BouteilleMailer.created(bouteilles(:one))
    assert_equal "Bouteille  crée avec succès", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["laloutre@qoqa.ch"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
