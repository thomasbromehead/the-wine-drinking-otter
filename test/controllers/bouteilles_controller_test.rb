require "test_helper"

class BouteillesControllerTest < ActionDispatch::IntegrationTest
  fixtures(:bouteilles)
  fixtures(:chateaus)
  include ActiveJob::TestHelper

  def setup
    @bouteille = bouteilles(:one)
    @chateau = chateaus(:one)
  end

  test "should get show" do
    get bouteille_url(bouteilles(:one))
    assert_response :success
    assert_match "#{bouteilles(:one).name}", @response.body
  end

  test "should be invalid if params doesn't contain 'bouteille'" do
    patch bouteille_url(@bouteille), params: {name: "New bouteille name"}.to_json,  headers: { "Content-Type": "application/json" }
    assert_response :error
    assert_response "", @response.body.message
    assert "text/javascript", @response.media_type
  end

  test "raises invalid content type" do
    patch bouteille_url(@bouteille), params: {bouteille: {name: "New bouteille name"}}
  end

  test "should update a bouteille" do
    patch bouteille_url(@bouteille), params: {"bouteille": {"name": "New bouteille name"}}.to_json, headers: { "Content-Type": "application/json" }
    assert_response :success
    assert_match "Bouteille updated successfully", @response.body
    assert_equal "application/json", @response.media_type
  end

  test "should get index" do
    get bouteilles_url
    assert_response :success
    assert_equal JSON.parse(@response.body).first.keys, JSON.parse(bouteilles.to_json).first.keys
  end

  test "Should send back the validation errors when creating a bottle" do
    post bouteilles_url, params: {name: "BOUTEILLE TEST",  chateau_id: @chateau.id }.to_json, headers: { "Content-Type": "application/json" }
    assert_equal 422, @response.status
    assert_match %{{"error":{"wine_type":["is not included in the list"]}}}, @response.body
  end

  test "Should create a bouteille" do
    perform_enqueued_jobs do
      assert_difference("Bouteille.count", 1) do
        post bouteilles_url, params: {
          bouteille: {
            name: "BOUTEILLE TEST",
            chateau_id: @chateau.id,
            wine_type: 1
          }
        }.to_json,  headers: { "Content-Type": "application/json" }
      end
      assert_response :success
      assert_match %{{\"success\":\"Bouteille Created\"}}, @response.body
      mail = ActionMailer::Base.deliveries.last
      assert_equal "Bouteille BOUTEILLE TEST crée avec succès", mail.subject
      assert_equal %w(laloutre@qoqa.ch), mail.from
      assert_equal %w(to@example.org), mail.to
    end
  end

  test "should raise ActionController::UnpermittedParameters  error when creating a bouteille" do
    assert_raises(ActionController::UnpermittedParameters) do
      post bouteilles_url, params: {bouteille: {other_attribute: false, name: "incorrect bottle", wine_type: 1, chateau_id: @chateau.id}}.to_json,  headers: { "Content-Type": "application/json" }
    end
  end

  test "should destroy bouteille" do
    assert_difference("Bouteille.count", -1) do
      delete bouteille_url(bouteilles(:two))
    end
    assert_response :success
  end
end
