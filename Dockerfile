FROM ruby:2.6.5
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main 9.5" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get update -yqq && apt-get install -yqq --no-install-recommends \
    npm \
    vim \
    libnotify-dev \
    libpq-dev postgresql \
    postgresql-client && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update -yqq && apt install  --no-install-recommends yarn && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    cat /etc/apt/sources.list.d/nodesource.list && \
    apt install -y nodejs &&\
    rm -rf /var/lib/apt/lists/*
ARG HOMEDIR=/usr/src/qoqa/
# Env variables recommended by bundler documentation
ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH
# Use volume as bundle cache so gems install faster
WORKDIR $HOMEDIR
COPY Gemfile* $HOMEDIR
RUN gem install bundler:2.2.1
RUN bundle install
EXPOSE 3000
# Copy the rest of the app into /usr/src/qoqa
COPY . $HOMEDIR



